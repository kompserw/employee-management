package pl.ksml.braintri.employee.management.pl.ksml.braintri.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Position;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Primary
@Repository
public class PositionDaoDb implements PositionDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Position> getList() {
        return (List<Position>) entityManager.createQuery("FROM Position p").getResultList();
    }

    @Override
    public void save(Position position) {

    }

    @Override
    public Position read(int id) {
        return null;
    }

    @Override
    public void remove(int id) {

    }

    @Override
    public void update(Position position) {

    }

    @Override
    public Position findPosition(int id) {
        return entityManager.find(Position.class, id);
    }
}
