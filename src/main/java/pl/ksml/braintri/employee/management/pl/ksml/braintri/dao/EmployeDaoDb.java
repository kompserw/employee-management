package pl.ksml.braintri.employee.management.pl.ksml.braintri.dao;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Employe;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Primary
@Transactional
public class EmployeDaoDb implements EmployeDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Employe> getList() {
        return (List<Employe>) entityManager.createQuery("FROM Employe e").getResultList();
    }

    @Override
    public void save(Employe employe) {
        entityManager.persist(employe);
    }


    @Override
    public void remove(int id) {
        Query query = entityManager.createQuery("DELETE Employe WHERE id = :number");
        query.setParameter("number",id);
        query.executeUpdate();
    }

    @Override
    public void update(Employe employe) {
        entityManager.merge(employe);
    }


    @Override
    public List<Employe> findByQuery(String searchedValue) {
        Query query = entityManager.createQuery("FROM Employe e WHERE LOWER(e.name) LIKE :q or LOWER(e.surname) LIKE :q");
        query.setParameter("q",'%' + searchedValue.toLowerCase() + '%');
        List<Employe> resultList = (List<Employe>) query.getResultList();
        return resultList;
    }

    @Override
    public Employe findEmploye(int id) {
        return entityManager.find(Employe.class,id);
    }
}
