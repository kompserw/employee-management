package pl.ksml.braintri.employee.management.pl.ksml.braintri.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="EMPLOYE")
public class Employe {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name="NAME")
    @NotNull
    @NotEmpty(message="Pole imię nie może być puste")
    @Size(min = 2, message="Rozmiar tekstu ma być powyżej 2 liter")
    private String name;

    @Column(name="SURNAME")
    @NotNull
    @NotEmpty(message="Pole nazwisko nie może być puste")
    @Size(min = 2, message="Rozmiar tekstu ma być powyżej 2 liter")
    private String surname;

    @Column(name="EMAIL")
    @NotNull
    @NotEmpty(message="Pole e-mail nie może być puste")
    @Size(min = 6, message="Rozmiar tekstu ma być powyżej 6 liter")
    private String email;

    @ManyToOne(cascade = CascadeType.ALL)
    private Position position;

    public Employe() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", position=" + position +
                '}';
    }
}
