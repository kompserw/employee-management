package pl.ksml.braintri.employee.management.pl.ksml.braintri.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.dao.EmployeDao;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.dao.PositionDao;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Employe;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Position;

import java.util.List;

@Service
public class EmployeServicesImpl implements EmployeServices {

    @Autowired
    private EmployeDao employeDao;

    @Autowired
    private PositionDao positionService;

    @Override
    public List<Employe> getList() {

        return employeDao.getList();
    }

    @Override
    public void save(Employe employe) {
        Position position = positionService.findPosition(employe.getPosition().getId());
        employe.setPosition(position);
        if(employe.getId() == 0){
            employeDao.save(employe);
        }else{
            employeDao.update(employe);
        }
    }

    @Override
    public Employe read(int id) {
        return employeDao.findEmploye(id);
    }

    @Override
    public void remove(int id) {
        employeDao.remove(id);
    }

    @Override
    public void update(Employe employe) {

    }

    @Override
    public List<Employe> findByQuery(String query) {
        return employeDao.findByQuery(query);
    }
}
