package pl.ksml.braintri.employee.management.pl.ksml.braintri.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.services.EmployeServices;


@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private EmployeServices employeServices;

    @GetMapping
    public ModelAndView searchBooks(@RequestParam("query") String query) {
        System.out.println(query);
        ModelAndView mav = new ModelAndView("list-employe");
        mav.addObject("list", employeServices.findByQuery(query));
        mav.addObject("searchQuery", query);
        return mav;
    }
}