package pl.ksml.braintri.employee.management.pl.ksml.braintri.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.dao.PositionDao;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Position;

import java.util.List;

@Service
public class PositionServicesImpl implements PositionServices {

    @Autowired
    public PositionDao positionDao;

    @Override
    public List<Position> getList() {
        return positionDao.getList();
    }

    @Override
    public void save(Position position) {

    }

    @Override
    public Position read(int id) {
        return null;
    }

    @Override
    public void remove(int id) {

    }

    @Override
    public void update(Position position) {

    }
}
