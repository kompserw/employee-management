package pl.ksml.braintri.employee.management.pl.ksml.braintri.services;

import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Position;

import java.util.List;

public interface PositionServices {

    List<Position> getList();
    void save(Position position);
    Position read(int id);
    void remove(int id);
    void update(Position position);

}
