package pl.ksml.braintri.employee.management.pl.ksml.braintri.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.configuration.SecurityConfiguration;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Employe;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.services.EmployeServices;
import pl.ksml.braintri.employee.management.pl.ksml.braintri.services.PositionServices;

import javax.validation.Valid;


@RequestMapping("/emp")
@Controller
public class EmployeController {

    @Autowired
    private EmployeServices employeServices;

    @Autowired
    private PositionServices positionServices;

    @Autowired
    private SecurityConfiguration securityConfiguration;

    @GetMapping
    public ModelAndView getEmploye() {
        ModelAndView mav = new ModelAndView("list-employe");
        mav.addObject("list", employeServices.getList());
        return mav;
    }

    @Secured("ROLE_USER")
    @GetMapping("/add")
    public ModelAndView addEmployeForm(){
        ModelAndView mav = new ModelAndView("add-employe");
        mav.addObject("employe",new Employe());
        mav.addObject("position",positionServices.getList());
        return mav;
    }

    @PostMapping("/add")
    public ModelAndView saveBook(@Valid @ModelAttribute("employe") Employe employe,
                                 BindingResult bindingResult) {
        System.out.println(employe);
        ModelAndView mav = new ModelAndView();
        mav.addObject("position",positionServices.getList());

        if (bindingResult.hasErrors()) {
            mav.addObject("employe", employe);
            mav.setViewName("add-employe");
        } else {
            employeServices.save(employe);
            mav.setViewName("redirect:/emp");
        }
        return mav;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/edit/{id}")
    public ModelAndView editEmployeForm(@PathVariable("id") int employeId) {
        ModelAndView mav = new ModelAndView("add-employe");
        mav.addObject("employe", employeServices.read(employeId));
        mav.addObject("position", positionServices.getList());
        return mav;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/remove/{id}")
    public String deleteEmploye(@PathVariable("id") int employeId) {
        employeServices.remove(employeId);
        return "redirect:/emp";
    }

    @GetMapping("/{id}")
    public ModelAndView getDetails(@PathVariable("id") int employeId) {
        return new ModelAndView("details-employe","employe", employeServices.read(employeId));
    }

    @GetMapping("/login")
    public ModelAndView login() {
        System.out.println("jestem");
        ModelAndView mav = new ModelAndView("login-employe");
//        mav.setViewName("login-employe");
        return mav;
    }

}
