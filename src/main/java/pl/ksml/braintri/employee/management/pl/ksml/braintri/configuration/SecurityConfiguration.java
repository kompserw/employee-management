package pl.ksml.braintri.employee.management.pl.ksml.braintri.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    // sample users
    @Autowired
    public void configureAuth(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("marcin").password("123").roles("USER")
                .and()
                .withUser("admin").password("321").roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/emp").permitAll()
                .antMatchers("/remove/**").hasRole("ADMIN")
                .antMatchers("/add/**").hasRole("USER")
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login")
                .and().formLogin().defaultSuccessUrl("/emp")
                .and().logout().logoutSuccessUrl("/emp");
    }
}