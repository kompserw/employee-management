package pl.ksml.braintri.employee.management.pl.ksml.braintri.dao;

import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Position;

import java.util.List;

public interface PositionDao {

    List<Position> getList();
    void save(Position position);
    Position read(int id);
    void remove(int id);
    void update(Position position);
    Position findPosition(int id);
}
