package pl.ksml.braintri.employee.management.pl.ksml.braintri.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name="POSITION")
public class Position {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name="NAME")
    @NotNull
    @NotEmpty(message="Pole stanowisko pracy nie może być puste")
    @Size(min = 2, message="Rozmiar tekstu ma być powyżej 2 liter")
    private String name;

    @OneToMany(mappedBy = "position")
    private List<Employe> employes;

    public Position() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public void setEmployes(List<Employe> employes) {
        this.employes = employes;
    }

    @Override
    public String toString() {
        return "Position{" +
                "name='" + name + '\'' +
                ", employes=" + employes +
                '}';
    }
}
