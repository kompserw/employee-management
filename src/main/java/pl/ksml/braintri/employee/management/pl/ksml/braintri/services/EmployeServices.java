package pl.ksml.braintri.employee.management.pl.ksml.braintri.services;

import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Employe;

import java.util.List;

public interface EmployeServices {

    List<Employe> getList();
    void save(Employe employe);
    Employe read(int id);
    void remove(int id);
    void update(Employe employe);
    List<Employe> findByQuery(String query);

}
