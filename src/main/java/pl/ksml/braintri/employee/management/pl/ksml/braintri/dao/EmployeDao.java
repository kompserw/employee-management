package pl.ksml.braintri.employee.management.pl.ksml.braintri.dao;

import pl.ksml.braintri.employee.management.pl.ksml.braintri.model.Employe;

import java.util.List;

public interface EmployeDao {

    List<Employe> getList();
    void save(Employe employe);
    void remove(int id);
    void update(Employe employe);
    List<Employe> findByQuery(String query);
    Employe findEmploye(int id);
}
