INSERT INTO POSITION (id, name) VALUES (1, 'Marketing');
INSERT INTO POSITION (id, name) VALUES (2, 'Finance');
INSERT INTO POSITION (id, name) VALUES (3, 'Accounting');
INSERT INTO POSITION (id, name) VALUES (4, 'Human Resource');

INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (1, 'Jan','Kowalski', 'jan@test.pl' , 1);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (2, 'Janina','Frołow', 'janina@test.pl' , 1);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (3, 'Jarek','Tyrek', 'jarek@test.pl' , 2);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (4, 'Ewa','Trzaska', 'ewa@test.pl' , 2);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (5, 'Tomek','Kociś', 'tomek@test.pl' , 3);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (6, 'Iza','Kotowska', 'iza@test.pl' , 3);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (7, 'Ola','Kowalska', 'ola@test.pl' , 4);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (8, 'Marcin','Jurczyk', 'marcin@test.pl' , 4);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (9, 'Teresa','Kozłowska', 'teresa@test.pl' , 1);
INSERT INTO EMPLOYE (id, name, surname, email, position_id) VALUES (10, 'Bożena','Nowak', 'bozena@test.pl' , 2);
